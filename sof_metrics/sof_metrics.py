# Author: Gabriel Chaviaras <chaviaras10@gmail.com>

"""
This file is the main file of sof_metrics project. Is responsible for
argument parsing and some checking. After successfully checking calls the
Retriever and TableGenerator classes for retriever and generating the metrics
taken from stackoverflow's API.
"""

from getopt import getopt, GetoptError
from sys import argv
from os import path
from retriever import Retriever
from table_gen import TableGenerator


def print_usage():
    print(argv[0] +
          ' -f "YYYY/MM/DD HH:MM:SS"' +
          ' -t "YYYY/MM/DD HH:MM:SS"' +
          ' -o <output file>'
          )

if __name__ == '__main__':
    try:
        try:
            opts, args = getopt(argv[1:], 'f:t:o:', ['from=', 'to=', 'out='])
        except GetoptError:
            print_usage()
            exit(2)

        output_file = None

        # extract the arguments from the command line
        for opt, arg in opts:
            if opt in ('-f', '--from'):
                from_date = arg
            elif opt in ('-t', '--to'):
                to_date = arg
            elif opt in ('-o', '--out'):
                output_file = arg

        if 'from_date' not in locals() or 'to_date' not in locals():
            print_usage()
            exit(2)

        # check if the custom path is valid
        if not output_file:     # if path is not defined
            output_file = path.abspath('../results.html')
        else:
            output_file = path.abspath(path.expanduser(output_file))
            # if path is invalid go to default
            if not path.isdir(path.dirname(output_file)):
                output_file = path.abspath('../results.html')
                print('Dir does not exists.')
                print('Using ' + output_file)

        retriever = Retriever(from_date, to_date)
        TableGenerator(
            container=retriever.get_data(),
            from_date=from_date,
            to_date=to_date,
            output_file=output_file
        ).gen_output()

        exit(True)

    except KeyboardInterrupt:
        exit(False)