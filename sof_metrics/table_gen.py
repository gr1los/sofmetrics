# Author: Gabriel Chaviaras <chaviaras10@gmail.com>

"""
This file includes a class that is responsible for generating a HTML file
that includes the metrics taken from the Retriever class.
"""

from itertools import izip_longest
from datetime import datetime
from codecs import open
from os import path
from jinja2 import Environment, FileSystemLoader


class TableGenerator:
    def __init__(self, container, from_date, to_date, output_file):
        """
        The constructor of TableGenerator class.

        :param container: Includes calculated data
        :type container: dict
        :param from_date: A representation of starting date
        :type from_date: string
        :param to_date: A representation of ending date
        :type to_date: string
        :param output_file: The output file directory
        :type output_file: string
        """
        self.accepted_answers = [container['accepted_answers']]
        self.avg_score_accepted = [container['avg_score_accepted']]
        self.avg_cnt_per_q = [container['avg_cnt_per_q']]
        self.answer_ids = container['answer_ids']
        self.comment_cnt = container['comment_cnt']
        self.from_date = from_date
        self.to_date = to_date
        self.output_file = output_file

        self.curDir = path.abspath(
            path.join(path.dirname(__file__), '..', 'templates'))

        self.template_file = 'template.jinja2'

    def gen_output(self):
        """
        This functions inserts and generates the HTML file output including
        the metrics taken from the Retriever class.
        The output path of the file is defined from the self.output_file var.
        """
        print('Generating html output file.')

        # Create the jinja2 environment.
        templ_env = Environment(
            loader=FileSystemLoader(self.curDir),
            trim_blocks=True                        # controls whitespaces
        )

        target = open(self.output_file, 'w', 'utf-8')
        target.write(templ_env.get_template(self.template_file).render(
            container=izip_longest(self.accepted_answers,
                                   self.avg_score_accepted,
                                   self.avg_cnt_per_q,
                                   self.answer_ids,
                                   self.comment_cnt,
                                   fillvalue=''),
            from_date=self.from_date,
            to_date=self.to_date,
            cur_time=datetime.utcnow()
        ))
        target.close()
