# Author: Gabriel Chaviaras <chaviaras10@gmail.com>

"""
This file includes a class that is responsible for retrieving data from
stackoverflow answers API. Also calculates some metrics about the
answers that created on the specific datetime range. The metrics are:
- Total number of accepted answers
- Average score for all the accepted answers
- Average answer count per question
- For the top 10 answers, ordered by score descending, find the
  comment count for every answer
"""

import requests
import sys

from time import sleep
from calendar import timegm
from dateutil import parser as date_parse
from collections import OrderedDict


class Retriever:
    def __init__(self, from_date, to_date):
        """
        The constructor of Retriever Class

        :param from_date: A representation of the start date
        :type from_date: string
        :param to_date: A representation of the end date
        :type to_date: string
        """
        # self.from_date = int(
        #     timegm(datetime.strptime(from_date, '%d/%m/%Y').timetuple()))
        # self.to_date = int(
        #     timegm(datetime.strptime(to_date, '%d/%m/%Y').timetuple()))

        self.from_date = int(timegm(
            date_parse.parse(from_date, fuzzy=True).timetuple()
        ))
        self.to_date = int(timegm(
            date_parse.parse(to_date, fuzzy=True).timetuple()
        ))

        self.apiUrl = 'https://api.stackexchange.com/2.2/answers'
        self.site = 'stackoverflow'

    def get_data(self):
        """
        This function returns data taken from stackoverflow site

        :return: A container with all data that need to be printed.
        :rtype: dict
        """
        params = {
            'todate': self.to_date,
            'fromdate': self.from_date,
            'site': self.site,
            'sort': 'votes',                # sort using votes/score
            'order': 'desc',                # sort descending
            'pagesize': 100,                # 100 = max page size
            'page': 1
        }

        total_entries = self.get_total()
        # the first part of the sum is the integer number of pages
        # the second part evaluates to True(1) if there is a remainder
        # and to False(0) if there is not.
        total_pages = int(total_entries / params['pagesize']) +\
            (total_entries % params['pagesize'] > 0)

        accepted_answers = 0
        score_sum = 0
        comment_cnt = dict()    # comment_cnt[answer_id] = answer's comment cnt
        question_map = dict()   # question_map[question_id] = num of acc answers

        print('Loading data from Stackoverflow.')

        # stackoverflow API returns results in segments,
        # so we need a do-while block and we check the field response[has_mode].
        # When it's True we move on the next page of results with a new
        # request. Else when it's False we stop the while True loop.
        while True:
            # print page progress
            sys.stdout.write(
                '\r' + 'Loading Page: ' +
                str(params['page']) + '/' + str(total_pages)
            )
            sys.stdout.flush()

            try:
                response = requests.get(self.apiUrl, params).json()

                # get the first 10 answer ids of response's first page only
                if params['page'] == 1:
                    ids = [x['answer_id'] for x in response['items'][:10]]
                    comment_cnt = self.get_comment_cnt(ids)

                # iterate through answers
                for item in response['items']:

                    # count accepted answers and score sum
                    if item['is_accepted']:
                        accepted_answers += 1
                        score_sum += item['score']  # only accepted have score

                    # map each question id with the number of answers
                    if item['question_id'] in question_map:     # update val
                        question_map[item['question_id']] += 1
                    else:
                        question_map[item['question_id']] = 1   # init val

                # check if we need to back off
                self.check_backoff(response)

                # go to the next page
                params['page'] += 1

                # check if this is the last page
                if not response['has_more']:
                    break
            except Exception as inst:
                print('Exception' + str(inst))
                print(response)

        print(' -->Done')
        return {
            'accepted_answers': accepted_answers,
            'avg_score_accepted': (
                (score_sum / accepted_answers) if accepted_answers else 0
            ),
            'avg_cnt_per_q': (
                sum(list(question_map.values())) /
                len(list(question_map.keys()))
                if len(list(question_map.keys())) else 0
            ),
            'answer_ids': comment_cnt.keys(),
            'comment_cnt': comment_cnt.values()
        }

    def get_total(self):
        """
        Requests and returns the total amount of answers that found for the
        specific datetime range. It uses the total filter defined in
        stackoverflow API.

        :return: total answer entries
        :rtype: int
        """
        params = {
            'todate': self.to_date,
            'fromdate': self.from_date,
            'site': self.site,
            'filter': 'total'
        }

        try:
            response = requests.get(self.apiUrl, params).json()

            # check if we need to back off
            self.check_backoff(response)

            return response['total']
        except Exception as inst:
            print('Exception ' + str(inst))
            print(response)

    def get_comment_cnt(self, answer_ids):
        """
        Requests and returns a (ordered)dictionary. The key represents the
        question id and the value the comment count of the corresponding
        answer. It uses the total filter defined in stackoverflow API.

        :param answer_ids:
        :type answer_ids: list
        :return: A dictionary including the comment count for each answer
        :rtype: OrderedDict
        """
        params = {
            'todate': self.to_date,
            'fromdate': self.from_date,
            'site': self.site,
            'filter': 'total'
        }

        comment_cnt = OrderedDict()

        # iterate through answer_ids
        for _id in answer_ids:
            try:
                response = requests.get(
                    self.apiUrl + '/' + str(_id) + '/comments',
                    params
                ).json()

                # check if we need to back off
                self.check_backoff(response)

                comment_cnt[_id] = response['total']

            except Exception as inst:
                print('Exception ' + str(inst))
                print(response)

        return comment_cnt

    def check_backoff(self, response):
        """
        Checks when backoff variable is present inside response.
        When it's there then the function sleeps for as much as the variable
        indicates.

        :param response: The response taken from request
        :type response: dict(json)
        """

        if 'backoff' in response:
            print('\nStaying back for ' + str(response['backoff']) + ' seconds')
            sleep(response['backoff'])

    def print_data(self, container):
        """
        A simple function to print is not a very good format the data
        taken from stackoverflow api.

        :param container: A container including metrics from API
        :type container: dict
        """
        accepted_answers = container['accepted_answers']
        score_sum = container['score_sum']
        question_map = container['question_map']
        comment_cnt = container['comment_cnt']

        print('\nAccepted Answers: ' + str(accepted_answers))
        print('Average score of accepted answers: ' +
              str((score_sum / accepted_answers) if accepted_answers else 0))
        print('Average answer count per question: ' +
              str(sum(list(question_map.values())) /
                  len(list(question_map.keys()))
                  if len(list(question_map.keys())) else 0))

        print('Top 10 answers with comment count')
        for key in comment_cnt:
            print('Answer ID = ' + str(key) +
                  ' Comment Count = ' + str(comment_cnt[key]))
