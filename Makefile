default: install
.PHONY: clean

install:
		pip2.7 install -r requirements.txt
		
run: clean
		python sof_metrics/sof_metrics.py ${ARGS}
		
test: clean
		python tests/retriever_test.py
		python tests/table_gen_test.py
		
clean:
		find . -name \*.pyc -delete
		find . -name \*~ -delete