# Sof-Metrics (Stackoverflow Metrics)

### What does it do?
* Answer retrieval for specific datetime range.
* Provides metric data using jinja2 templates.
* Metrics data includes :
    * Total number of accepted answers.
    * Average score for all the accepted answers.
    * Average answer count per question.
    * Comment count for the 10 top answers, ordered by score descending.

### How to install?
* Use python **2.7** only!
* Install Dependencies
    * python-dateutil
    * jinja2
    * requests
* Using `pip install -r requirements.txt`


### How to use?
* Install the project's requirements
* `python sof_metrics/sof_metrics.py 
-f "start datetime" 
-t "end datetime" 
-f "output file"`
* e.g. `python sof_metrics/sof_metrics.py 
-f "2012/1/10 10:10" 
-t "2012/1/11 23:20" 
-o ~/out.html`

### How to run test cases?
* `make test`

**Attention:** Due to StackExchange's API throttle 
control do not make an extreme use of this program. 
After each execution it is recommended to wait at 
least one hour. After an extreme usage you have to 
wait for 1 day for your IP ban to be removed from the API 
server. 
More info [here](https://api.stackexchange.com/docs/throttle).

### Makefile rules:
* make install
* make run
* make test
* make clean
