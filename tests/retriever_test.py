import unittest
from os import path as opath
from sys import path as spath


class TestingRetrieverMethods(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestingRetrieverMethods, self).__init__(*args, **kwargs)
        package_path = opath.abspath(
            opath.join(
                opath.dirname(__file__), '..', 'sof_metrics')
        )
        spath.append(package_path)
        import retriever

        self.from_date = '2011/06/01'
        self.to_date = '2011/06/02'

        self.retriever = retriever.Retriever(
            self.from_date,
            self.to_date
        )

    def test1_check_get_data(self):
        container = self.retriever.get_data()
        self.assertTrue('accepted_answers' in container)
        self.assertTrue('avg_score_accepted' in container)
        self.assertTrue('avg_cnt_per_q' in container)
        self.assertTrue('answer_ids' in container)
        self.assertTrue('comment_cnt' in container)

    def test2_check_get_total(self):
        resp = self.retriever.get_total()
        self.assertTrue('total' in resp)

    def test3_check_get_comment_cnt(self):
        ids = [8809472, 8808453, 8801990]
        container = self.retriever.get_comment_cnt(ids)
        self.assertTrue(len(container.keys()) != 0)
        self.assertTrue(len(container.values()) != 0)

if __name__ == '__main__':
    unittest.main()
