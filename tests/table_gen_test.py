import unittest
from os import path as opath
from sys import path as spath


class TestingTableGeneratorMethods(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestingTableGeneratorMethods, self).__init__(*args, **kwargs)
        package_path = opath.abspath(
            opath.join(
                opath.dirname(__file__), '..', 'sof_metrics')
        )
        spath.append(package_path)
        import table_gen

        self.container = {
            'accepted_answers': 50,
            'avg_score_accepted': 50/10,
            'avg_cnt_per_q': 100,
            'answer_ids': [8809472, 8808453, 8801990],
            'comment_cnt': [10, 33, 1]
        }

        self.from_date = '01/06/2011'
        self.to_date = '02/06/2011'
        self.output_file = 'test.html'

        self.generator = table_gen.TableGenerator(
            container=self.container,
            from_date=self.from_date,
            to_date=self.to_date,
            output_file=self.output_file
        )

    def test1_check_gen_output(self):
        self.generator.gen_output()
        self.assertTrue(opath.isfile(self.output_file))

if __name__ == '__main__':
    unittest.main()
